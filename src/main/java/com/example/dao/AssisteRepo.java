package com.example.dao;

import com.example.entities.Assiste;
import com.example.entities.AssisteKey;
import com.example.entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssisteRepo extends JpaRepository<Assiste, AssisteKey> {

    List<Assiste> findByUtilisateurOrderByTrajetDate(Utilisateur utilisateur);

}
