package com.example.dao;

import com.example.entities.Utilisateur;
import com.example.entities.Voiture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.awt.*;
import java.util.List;

@RepositoryRestResource
@CrossOrigin("*")
public interface VoitureRepo extends JpaRepository<Voiture,String> {
    List<Voiture> findByOwner(Utilisateur owner);
}
