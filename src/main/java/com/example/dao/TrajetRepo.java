package com.example.dao;

import com.example.entities.Trajet;
import com.example.entities.Utilisateur;
import com.example.entities.Voiture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.time.LocalDate;
import java.util.List;

@RepositoryRestResource
@CrossOrigin("*")
public interface TrajetRepo extends JpaRepository<Trajet,Long> {

    List<Trajet> findByVilleDAndVilleAAndDate(String villeD, String villeA, LocalDate date);

    List<Trajet> findByCarOwner(Utilisateur owner);
}
