package com.example.web;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.dao.UtilisateurRepo;
import com.example.entities.Role;
import com.example.entities.Utilisateur;
import com.example.service.UtilisateurService;
import com.example.utilitis.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
public class UtilisateurController {
    private UtilisateurService service;
    private UtilisateurRepo utilisateurRepo;
    private PasswordEncoder encoder;
    private EmailSenderService emailService;

    @GetMapping(path = "/refreshToken")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authToken = request.getHeader(JWTUtilities.AUTH_HEADER);
        if (authToken != null && authToken.startsWith(JWTUtilities.PREFIX)) {
            try {
                String jwt = authToken.substring(7);
                Algorithm algorithm = Algorithm.HMAC256(JWTUtilities.SECRET);
                JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                jwtVerifier.verify(jwt);
                DecodedJWT decodedJWT = jwtVerifier.verify(jwt);
                String username = decodedJWT.getSubject();
                Utilisateur utilisateur = service.loadUserByUsername(username);
                String access_token = JWT.create()
                        .withSubject(utilisateur.getEmail())
                        .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtilities.EXPIRE_ACCESS_TOKEN))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles", utilisateur.getRoles().stream().map(r -> r.getName()).collect(Collectors.toList()))
                        .sign(algorithm);
                Map<String, String> tokens = new HashMap<>();
                tokens.put("access_token", access_token);
                tokens.put("refresh_token", jwt);
                response.setContentType("application/json");
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            } catch (Exception e) {
                response.setHeader("Error_Message", e.getMessage());
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        } else
            throw new RuntimeException("Error");
    }

    @GetMapping(path = "/admin/utilitsateurs")
    public List<Utilisateur> getAllUsers() {
        return service.listUsers();
    }

    @PostMapping(path = "/user/confirmUser")
    public Utilisateur confirmUser(Principal principal, @RequestParam(name = "token") String token) {
        Utilisateur user = service.loadUserByUsername(principal.getName());
        if (user.getToken().equals(token) &&
                user.getExpireAt() > System.currentTimeMillis()) {
            user.setVerified(true);
            return utilisateurRepo.save(user);
        } else
            generateToken(principal);
        return user;

    }

    @PostMapping(path = "/user/generateToken")
    public Utilisateur generateToken(Principal principal) {
        Utilisateur user = service.loadUserByUsername(principal.getName());
        user.setToken("" + ((int) (Math.random() * 9000) + 1000));
        user.setExpireAt(System.currentTimeMillis() + 5 * 60 * 1000);
        return utilisateurRepo.save(user);
    }

    @GetMapping(path = "/all/demandeConfirmation/{email}")
    public ResponseEntity<?> demandeChangePassword(@PathVariable String email) {
        Utilisateur user = service.loadUserByUsername(email);
        if (user != null) {
            user.setToken("" + ((int) (Math.random() * 9000) + 100000));
            user.setExpireAt(System.currentTimeMillis() + 5 * 60 * 1000);
            emailService.sendResetPassword(email, user.getToken());
            utilisateurRepo.save(user);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/all/verifyConfirmation/{email}")
    public ResponseEntity<?> verifyConfirmation(@PathVariable String email, @RequestParam String code) {
        Utilisateur user = service.loadUserByUsername(email);
        if (user.getToken().equals(code) &&
                user.getExpireAt() > System.currentTimeMillis()) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(path = "/all/changePasswordAfterConfirmation/{email}")
    public ResponseEntity<?> changePasswordAfterConfirmation(@PathVariable String email, @RequestBody PasswordChangeForm p,@RequestParam String code) {
        Utilisateur user = service.loadUserByUsername(email);
        if (user.getToken().equals(code) && user.getExpireAt() > System.currentTimeMillis()) {
            user.setId(user.getId());
            user.setPassword(encoder.encode(p.getNewPassword()));
            utilisateurRepo.save(user);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/admin/utilisateur/{email}")
    public Utilisateur getUserByEmail(@PathVariable String email) {
        return service.loadUserByUsername(email);
    }

    @GetMapping(path = "/user/profile")
    public Utilisateur getLogedUser(Principal user) {
        Utilisateur utilisateur = service.loadUserByUsername(user.getName());
        return utilisateur;
    }

    @PostMapping(path = "/all/utilisateur")
    public ResponseEntity<Utilisateur> addUserWithoutImage(@RequestBody Utilisateur u) {
        try {
            return ResponseEntity.ok(service.addNewUser(u));
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @PostMapping(path = "/admin/role")
    public Role addRole(@RequestBody Role role) {
        return service.addNewRole(role);
    }

    @PostMapping(path = "/admin/addRoleToUser")
    public void addRoleToUser(@RequestBody RoleUserForm form) {
        service.addRoleToUser(form.getEmail(), form.getRole());
    }

    @PostMapping(path = "/user/changePassword")
    public ResponseEntity<Utilisateur> changePassword(Principal user, @RequestBody PasswordChangeForm passwordChangeForm) {
        Utilisateur utils = service.loadUserByUsername(user.getName());
        if (service.checkPassword(user.getName(), passwordChangeForm.getOldPassword())) {
            utils.setPassword(encoder.encode(passwordChangeForm.getNewPassword()));
            return ResponseEntity.ok(utilisateurRepo.save(utils));
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
    }

    @PutMapping(path = "/admin/utilisateur/{id}")
    public Utilisateur modifyUtilisateur(@PathVariable Long id, @RequestBody Utilisateur u) {
        u.setId(id);
        return utilisateurRepo.save(u);
    }

    @PutMapping(path = "/user/updateLogged")
    public ResponseEntity<Utilisateur> modifyLoggedUser(Principal principal, @RequestBody modifyLoggedForm u, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (principal.getName().equals(u.getEmail())) {
            Utilisateur user = service.loadUserByUsername(principal.getName());
            service.modifyLogged(user, u);
            service.buildTokenAfterUpdate(u, request, response);
            System.out.println("done");
            return ResponseEntity.ok(user);
        } else {
            Utilisateur user = service.loadUserByUsername(u.getEmail());
            if (user == null) {
                user = service.loadUserByUsername(principal.getName());
                service.modifyLogged(user, u);
                service.buildTokenAfterUpdate(u, request, response);
                System.out.println("done");
                return ResponseEntity.ok(user);
            }
            System.out.println("canceled");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @DeleteMapping(path = "/admin/utilisateur/{id}")
    public void deleteUser(@PathVariable Long id) {
        utilisateurRepo.deleteById(id);
    }

    @DeleteMapping(path = "/user/deletelogged")
    public void deleteLoggedUser(Principal principal) {
        Utilisateur user = service.loadUserByUsername(principal.getName());
        utilisateurRepo.deleteById(user.getId());
    }

    @GetMapping(path="/all/getUserById/{id}")
    public Utilisateur findUserById(@PathVariable long id){
        return utilisateurRepo.findById(id).get();
    }

}


