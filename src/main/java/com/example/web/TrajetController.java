package com.example.web;

import com.example.dao.TrajetRepo;
import com.example.dao.UtilisateurRepo;
import com.example.dao.VoitureRepo;
import com.example.entities.Assiste;
import com.example.entities.Trajet;
import com.example.entities.Utilisateur;
import com.example.entities.Voiture;
import com.example.service.TrajetService;
import com.example.service.UtilisateurService;
import com.example.utilitis.EmailSenderService;
import com.example.utilitis.TrajetSearchForum;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@AllArgsConstructor
public class TrajetController {

    private TrajetService trajetService;
    private UtilisateurService utilisateurService;
    private TrajetRepo trajetRepo;
    private UtilisateurRepo utilisateurRepo;
    private EmailSenderService senderService;

    @PostMapping(path = "/all/gettrajets")
    public List<Trajet> get_trajet(@RequestBody TrajetSearchForum form){
        return trajetService.trajetParVilleDetVilleAetdateD(form.getVilleD(),form.getVilleA(),form.getDate());
    }

    @GetMapping(path = "/all/findtrajet/{id}")
    public Trajet findTrajet(@PathVariable long id){
        return trajetRepo.findById(id).get();
    }

    @PostMapping(path="/user/addTrajet")
    public Trajet add_trajet(@RequestBody Trajet trajet){
        return trajetRepo.save(trajet);
    }

    @PutMapping(path = "/user/modifyTrajet/{id}")
    public Trajet modifyTrajet (@PathVariable long id,@RequestBody Trajet trajet){
        Trajet t = trajetRepo.findById(id).get();
        for (Assiste a : t.getAssistes()){
            senderService.modifiedTrajet(a.getUtilisateur().getEmail(),t);
        }
        trajet.setId(id);
        return trajetRepo.save(trajet);
    }

    @DeleteMapping(path = "/user/deleteTrajet/{id}")
    public void deleteTrajet(@PathVariable long id){
        Trajet t = trajetRepo.findById(id).get();
        for (Assiste a : t.getAssistes()){
            senderService.deletedTrajet(a.getUtilisateur().getEmail(),t);
        }
        trajetRepo.deleteById(id);
    }

    @GetMapping(path="/user/loadTrajetsOfLogged")
    public List<Trajet> findByLogged(Principal principal){
        return trajetService.trajetsofLogged(utilisateurService.loadUserByUsername(principal.getName()));
    }

    @GetMapping(path="/all/trajetsById/{id}")
    public List<Trajet> trajetById(@PathVariable long id){
        Utilisateur user = utilisateurRepo.findById(id).get();
        return trajetRepo.findByCarOwner(user);
    }
}


