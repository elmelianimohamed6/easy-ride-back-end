package com.example.web;

import com.example.dao.AssisteRepo;
import com.example.dao.UtilisateurRepo;
import com.example.entities.Assiste;
import com.example.entities.Trajet;
import com.example.entities.Utilisateur;
import com.example.service.AssisteService;
import com.example.service.UtilisateurService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@AllArgsConstructor
public class AssisteController {

    private AssisteService assisteService;
    private AssisteRepo assisteRepo;
    private UtilisateurService utilisateurService;
    private UtilisateurRepo utilisateurRepo;

    @PostMapping(path = "/user/addAssiste")
    public Assiste assAssiste(Principal principal, @RequestBody Trajet trajet){
        Utilisateur user=utilisateurService.loadUserByUsername(principal.getName());
        return assisteService.addAssiste(user,trajet);
    }

    @PutMapping(path = "/user/modifyAssiste/{id}")
    public ResponseEntity<Assiste> modifyAssiste(@RequestBody Trajet trajet,@RequestParam long code, @PathVariable long id){
        Utilisateur user=utilisateurRepo.findById(id).get();
        if(code==1){
            assisteService.acceptedAssiste(user,trajet);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }else if(code==0){
            assisteService.refuseAssiste(user,trajet);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }else{
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path="/user/getHistory")
    public List<Assiste> historyAssistes(Principal principal){
        return assisteRepo.findByUtilisateurOrderByTrajetDate(utilisateurService.loadUserByUsername(principal.getName()));
    }
}
