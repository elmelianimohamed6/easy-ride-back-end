package com.example.web;

import com.example.dao.VoitureRepo;
import com.example.entities.Utilisateur;
import com.example.entities.Voiture;
import com.example.service.UtilisateurService;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@AllArgsConstructor
public class VoitureController {

    private VoitureRepo voitureRepo;
    private UtilisateurService service;

    @PostMapping(path="/user/addCarToLoggedUser")
    public ResponseEntity<Voiture> addCarToLoggedUser(Principal user, @RequestBody Voiture voiture){
        Utilisateur utilisateur=service.loadUserByUsername(user.getName());
        try{
            voiture.setOwner(utilisateur);
            return ResponseEntity.ok(voitureRepo.save(voiture));
        }
        catch(Exception e){
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping(path="/user/findCarsOfLoggedUser")
    public List<Voiture> findCarsOfLoggedUser(Principal principal){
        return voitureRepo.findByOwner(service.loadUserByUsername(principal.getName()));
    }

    @GetMapping(path="/user/findCar/{mat}")
    public Voiture findCarByMat(@PathVariable String mat){
        return voitureRepo.findById(mat).get();
    }

    @PutMapping(path="/user/modifyCarOfLoggedUser/{mat}")
    public Voiture ModifyCarOfLoggedUser(@PathVariable String mat, @RequestBody Voiture voiture){
        Voiture car=voitureRepo.findById(mat).get();
        voiture.setOwner(car.getOwner());
        voiture.setMat(mat);
        return voitureRepo.save(voiture);
    }

    @DeleteMapping(path="/user/deleteCarOfLoggedUser/{mat}")
    public void deleteCarOfLoggedUser(@PathVariable String mat){
        voitureRepo.deleteById(mat);
    }

}
