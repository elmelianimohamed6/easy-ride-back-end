package com.example.utilitis;

import lombok.Data;

@Data
public
class PasswordChangeForm {
    private String oldPassword;
    private String newPassword;
}