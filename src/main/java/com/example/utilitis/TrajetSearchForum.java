package com.example.utilitis;

import lombok.Data;

import java.time.LocalDate;

@Data
public class TrajetSearchForum{
    private String villeA;
    private String villeD;
    private LocalDate date;
}
