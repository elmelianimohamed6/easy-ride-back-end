package com.example.utilitis;

import lombok.Data;

@Data
public class RoleUserForm {
    private String role;
    private String email;
}
