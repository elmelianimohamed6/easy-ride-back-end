package com.example.utilitis;

import com.example.entities.Trajet;
import com.example.entities.Utilisateur;
import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

@AllArgsConstructor
@Service
public class EmailSenderService {

    public boolean sendEmail(String subject, String message, String to) {
        boolean foo = false; // Set the false, default variable "foo", we will allow it after sending code process email

        String senderEmail = "elmelianimohamed6@gmail.com"; // your gmail email id
        String senderPassword = "edxewkkmdjsnzlbi"; // your gmail id password

        // Properties class enables us to connect to the host SMTP server
        Properties properties = new Properties();

        // Setting necessary information for object property

        // Setup host and mail server
        properties.put("mail.smtp.auth", "true"); // enable authentication
        properties.put("mail.smtp.starttls.enable", "true"); // enable TLS-protected connection
        properties.put("mail.smtp.host", "smtp.gmail.com"); // Mention the SMTP server address. Here Gmail's SMTP server is being used to send email
        properties.put("mail.smtp.port", "587"); // 587 is TLS port number

        // get the session object and pass username and password
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });

        try {

            MimeMessage msg = new MimeMessage(session); // Create a default MimeMessage object for compose the message

            msg.setFrom(new InternetAddress(senderEmail)); // adding sender email id to msg object

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); // adding recipient to msg object

            msg.setSubject(subject); // adding subject to msg object
            msg.setText(message); // adding text to msg object

            Transport.send(msg); // Transport class send the message using send() method
            System.out.println("Email Sent Wtih Attachment Successfully...");

            foo = true; // Set the "foo" variable to true after successfully sending emails

        } catch (Exception e) {

            System.out.println("EmailService File Error" + e);
        }

        return foo; // and return foo variable
    }


    public boolean sendResetPassword(String to, String code) {
        boolean foo = false; // Set the false, default variable "foo", we will allow it after sending code process email

        String senderEmail = "elmelianimohamed6@gmail.com"; // your gmail email id
        String senderPassword = "edxewkkmdjsnzlbi"; // your gmail id password

        // Properties class enables us to connect to the host SMTP server
        Properties properties = new Properties();

        // Setting necessary information for object property

        // Setup host and mail server
        properties.put("mail.smtp.auth", "true"); // enable authentication
        properties.put("mail.smtp.starttls.enable", "true"); // enable TLS-protected connection
        properties.put("mail.smtp.host", "smtp.gmail.com"); // Mention the SMTP server address. Here Gmail's SMTP server is being used to send email
        properties.put("mail.smtp.port", "587"); // 587 is TLS port number

        // get the session object and pass username and password
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });

        try {

            MimeMessage msg = new MimeMessage(session); // Create a default MimeMessage object for compose the message

            MimeMessageHelper helper = new MimeMessageHelper(msg, true); // create MimeMessageHelper class

            helper.setFrom(new InternetAddress(senderEmail)); // adding sender email id to helper object

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); //adding recipient to msg object

            helper.setSubject("Resest Password"); // adding subject to helper object

            MimeMultipart mimeMultipart = new MimeMultipart(); // create MimeMultipart object

            MimeBodyPart textMime = new MimeBodyPart(); // create first MimeBodyPart object textMime for containing the message

            MimeBodyPart messageBodyPart = new MimeBodyPart(); // create second MimeBodyPart object messageBodyPart for containing the html format data

            textMime.setText("");

            // create message within html format tag and assign to the content variable
            String content = "\n" +
                    "<!--EMAIL_CONFIRMATION_EN-->\n" +
                    "<!DOCTYPE html>\n" +
                    "<html lang=\"en\" class=\"miro\" style=\"background-color:#f3f4f8;font-size:0;line-height:0\">\n" +
                    "  <head xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta charset=\"UTF-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <title style=\"font-family:Helvetica,Arial,sans-serif\">Title</title>\n" +
                    "    <link rel=\"stylesheet\" href=\"../css/app.css\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "  </head>\n" +
                    "  <body style=\"-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#f5f5f5;background-color:#f3f4f8;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.43;margin:0;min-width:600px;padding:0;text-align:left;width:100%!important\">\n" +
                    "    <table class=\"miro__container\" align=\"center\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0;font-family:Helvetica,Arial,sans-serif;max-width:600px;min-width:600px;padding:0;text-align:left;vertical-align:top\">\n" +
                    "      <tr style=\"font-family:Helvetica,Arial,sans-serif;padding:0;text-align:left;vertical-align:top\">\n" +
                    "        <td class=\"miro__content-wrapper\" style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.43;margin:0;padding:0;padding-top:43px;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                    "          <div class=\"miro__content\" style=\"background-color:#fff;font-family:Helvetica,Arial,sans-serif\">\n" +
                    "\n" +
                    "            <div class=\"miro__content-body\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "              <div class=\"miro-title-block\" style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:40px 40px 36px\">\n" +
                    "                <div class=\"miro-title-block__title font-size-42\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">Complete registration</div>\n" +
                    "                <div class=\"miro-title-block__subtitle font-size-20 m-top-16\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;margin-top:16px;opacity:.6\">Please enter this confirmation code in the window where you started creating your\n" +
                    "                  account:</div>\n" +
                    "              </div>\n" +
                    "              <div class=\"miro-confirmation-code-block\" style=\"font-family:Helvetica,Arial,sans-serif;padding:0 40px\">\n" +
                    "                <div class=\"miro-confirmation-code-block__code\" style=\"background-color:#f3f4f8;border-radius:4px;color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:48px;font-stretch:normal;font-style:normal;font-weight:700;height:128px;letter-spacing:normal;line-height:128px;text-align:center\">"+code+"</div>\n" +
                    "              </div>\n" +
                    "              <div class=\"miro-title-block\" style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:10px 0 0 40px\">\n" +
                    "                <div class=\"miro-title-block__title font-size-42\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\"></div>\n" +
                    "                <div class=\"miro-title-block__subtitle font-size-20 m-top-16\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;margin-top:16px;opacity:.6\"></div>\n" +
                    "              </div>\n" +
                    "              <div class=\"miro-title-block\" style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:10px 0 12px 40px\">\n" +
                    "                <div class=\"miro-title-block__title font-size-42\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\"></div>\n" +
                    "              </div>\n" +
                    "              <table class=\"spacer\" style=\"border-collapse:collapse;border-spacing:0;font-family:Helvetica,Arial,sans-serif;padding:0;text-align:left;vertical-align:top;width:100%\">\n" +
                    "                <tbody style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "                  <tr style=\"font-family:Helvetica,Arial,sans-serif;padding:0;text-align:left;vertical-align:top\">\n" +
                    "                    <td height=\"0px\" style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:0;font-weight:400;hyphens:auto;line-height:0;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\"> </td>\n" +
                    "                  </tr>\n" +
                    "                </tbody>\n" +
                    "              </table>\n" +
                    "              <div class=\"miro-title-block\" style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:10px 0 23px 40px\">\n" +
                    "\n" +
                    "                <div class=\"miro-title-block__subtitle font-size-20 m-top-16\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;margin-top:16px;opacity:.6\"></div>\n" +
                    "              </div>\n" +
                    "              <div class=\"miro__sep\" style=\"background-color:#e1e0e7;font-family:Helvetica,Arial,sans-serif;height:1px\"></div>\n" +
                    "            </div>\n" +
                    "          </div>\n" +
                    "          <div class=\"miro__footer\" style=\"font-family:Helvetica,Arial,sans-serif;padding-bottom:72px;padding-top:42px\">\n" +
                    "            <div class=\"miro__footer-title\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.38;margin-top:0!important;opacity:.7;text-align:center\">You have received this notification because you have signed up\n" +
                    "              <br style=\"font-family:Helvetica,Arial,sans-serif\">for\n" +
                    "              <a href=\"/\" target=\"_blank\" style=\"Margin:0;color:inherit;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.43;margin:0;padding:0;text-align:left;text-decoration:none\">Miro</a>— endless online whiteboard for team collaboration.</div>\n" +
                    "          </div>\n" +
                    "        </td>\n" +
                    "      </tr>\n" +
                    "    </table>\n" +
                    "\n" +
                    "    <!-- prevent Gmail on iOS font size manipulation -->\n" +
                    "    <div style=\"display:none;font:15px courier;font-family:Helvetica,Arial,sans-serif;line-height:0;white-space:nowrap\">                                                                             \n" +
                    "                                               </div>\n" +
                    "  </body>\n" +
                    "</html>";

            // sets html format content to the messageBodyPart object
            messageBodyPart.setContent(content, "text/html; charset=utf-8");

            //The mimeMultipart adds textMime and messageBodypart to the
            mimeMultipart.addBodyPart(textMime);
            mimeMultipart.addBodyPart(messageBodyPart);
            msg.setContent(mimeMultipart);
            helper.setText("");
            msg.setText("");
            msg.setContent(mimeMultipart); // Set the mimeMultipart the contents of the msg

            Transport.send(msg); // Transport class send the message using send() method
            System.out.println("Email Sent With HTML Template Style Successfully...");

            foo = true; // Set the "foo" variable to true after successfully sending emails

        } catch (Exception e) {

            System.out.println("EmailService File Error" + e);
        }

        return foo; //and return foo variable
    }
/*
    public void sendEmailWithAttachment(String subject, String message, String toEmail) throws MessagingException {

        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper mimeMessageHelper
                = new MimeMessageHelper(mimeMessage, true);

        mimeMessageHelper.setFrom("elmelianimohamed6@gmail.com");
        mimeMessageHelper.setTo(toEmail);
        mimeMessageHelper.setText(message);
        mimeMessageHelper.setSubject(subject);

        Transport.send(mimeMessage);
        System.out.println("Mail Send...");

    }
*/

    public boolean sendDemandeAssiste(String to, Utilisateur user, Trajet trajet) {
        boolean foo = false; // Set the false, default variable "foo", we will allow it after sending code process email

        String senderEmail = "elmelianimohamed6@gmail.com"; // your gmail email id
        String senderPassword = "edxewkkmdjsnzlbi"; // your gmail id password

        // Properties class enables us to connect to the host SMTP server
        Properties properties = new Properties();

        // Setting necessary information for object property

        // Setup host and mail server
        properties.put("mail.smtp.auth", "true"); // enable authentication
        properties.put("mail.smtp.starttls.enable", "true"); // enable TLS-protected connection
        properties.put("mail.smtp.host", "smtp.gmail.com"); // Mention the SMTP server address. Here Gmail's SMTP server is being used to send email
        properties.put("mail.smtp.port", "587"); // 587 is TLS port number

        // get the session object and pass username and password
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });

        try {

            MimeMessage msg = new MimeMessage(session); // Create a default MimeMessage object for compose the message

            MimeMessageHelper helper = new MimeMessageHelper(msg, true); // create MimeMessageHelper class

            helper.setFrom(new InternetAddress(senderEmail)); // adding sender email id to helper object

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); //adding recipient to msg object

            helper.setSubject("Demande de participation"); // adding subject to helper object

            MimeMultipart mimeMultipart = new MimeMultipart(); // create MimeMultipart object

            MimeBodyPart textMime = new MimeBodyPart(); // create first MimeBodyPart object textMime for containing the message

            MimeBodyPart messageBodyPart = new MimeBodyPart(); // create second MimeBodyPart object messageBodyPart for containing the html format data

            // create message within html format tag and assign to the content variable
            textMime.setText("");

            String content="\n" +
                    "<!--EMAIL_CONFIRMATION_EN-->\n" +
                    "<!DOCTYPE html>\n" +
                    "<html lang=\"en\" class=\"miro\" style=\"background-color:#f3f4f8;font-size:0;line-height:0\">\n" +
                    "  <head xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta charset=\"UTF-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <title style=\"font-family:Helvetica,Arial,sans-serif\">Title</title>\n" +
                    "    <link rel=\"stylesheet\" href=\"../css/app.css\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "  </head>\n" +
                    "  <body style=\"-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#f5f5f5;background-color:#f3f4f8;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.43;margin:0;min-width:600px;padding:0;text-align:left;width:100%!important\">\n" +
                    "    <table class=\"miro__container\" align=\"center\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;border-spacing:0;font-family:Helvetica,Arial,sans-serif;max-width:600px;min-width:600px;padding:0;text-align:left;vertical-align:top\">\n" +
                    "      <tr style=\"font-family:Helvetica,Arial,sans-serif;padding:0;text-align:left;vertical-align:top\">\n" +
                    "        <td class=\"miro__content-wrapper\" style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.43;margin:0;padding:0;padding-top:43px;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                    "          <div class=\"miro__content\" style=\"background-color:#fff;font-family:Helvetica,Arial,sans-serif\">\n" +
                    "            <div class=\"miro__content-body\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "              <div class=\"miro-title-block\" style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:40px 40px 36px\">\n" +
                    "                <div class=\"miro-title-block__title font-size-42\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">Demande de participation </div>\n" +
                    "                <div class=\"miro-title-block__subtitle font-size-20 m-top-16\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;margin-top:16px;opacity:.6\">Vous avez reçu une nouvelle demande de participation a propos de votre trajet :<br/><br/>\n" +
                    "                  \n" +
                    "Ville depart : "+trajet.getVilleD()+"<br/><br/>\n" +
                    "Ville Arrive : "+trajet.getVilleA()+"<br/><br/>\n" +
                    "Date : "+trajet.getDate()+"<br/><br/>\n" +
                    "Heure : "+trajet.getHeure()+"\n" +
                    "                </div>\n" +
                    "              </div>\n" +
                    "              <div class=\"miro-title-block\" style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:0px 0 12px 40px\">\n" +
                    "                <div class=\"miro-title-block__title font-size-42\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\"></div>\n" +
                    "                <div style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.4;opacity:.6\" class=\"miro-title-block__subtitle font-size-20 m-top-16\">La demande a été envoyée par :<br/><br/>\n" +
                    "                  <a href=\"mailto:"+user.getEmail()+"\">"+user.getNom()+"</a>\n" +
                    "                  </div>\n" +
                    "              </div> \n" +
                    "              <div class=\"miro-title-block\" style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:10px 0 23px 40px\">\n" +
                    "                <div class=\"miro-title-block__title font-size-42\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\"></div>\n" +
                    "                <div class=\"miro-title-block__subtitle font-size-20 m-top-16\" style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;margin-top:16px;opacity:.6\">Afin d'améliorer notre plateforme merci de visiter votre plateforme afin d'accepter ou de refuser cette demande.</div>\n" +
                    "              </div>\n" +
                    "              <div class=\"miro__sep\" style=\"background-color:#e1e0e7;font-family:Helvetica,Arial,sans-serif;height:1px\"></div>\n" +
                    "            </div>\n" +
                    "          </div>\n" +
                    "          <div class=\"miro__footer\" style=\"font-family:Helvetica,Arial,sans-serif;padding-bottom:72px;padding-top:42px\">\n" +
                    "\n" +
                    "          </div>\n" +
                    "        </td>\n" +
                    "      </tr>\n" +
                    "    </table>\n" +
                    "  </body>\n" +
                    "</html>";

            // sets html format content to the messageBodyPart object
            messageBodyPart.setContent(content, "text/html; charset=utf-8");
            msg.setContent(mimeMultipart);
            helper.setText("");
            msg.setText("");
            //The mimeMultipart adds textMime and messageBodypart to the
            mimeMultipart.addBodyPart(textMime);
            mimeMultipart.addBodyPart(messageBodyPart);

            msg.setContent(mimeMultipart); // Set the mimeMultipart the contents of the msg

            Transport.send(msg); // Transport class send the message using send() method

            foo = true; // Set the "foo" variable to true after successfully sending emails

        } catch (Exception e) {

            System.out.println("EmailService File Error" + e);
        }

        return foo; //and return foo variable
    }

    public boolean confirmAssiste(String to, Trajet trajet) {
        boolean foo = false; // Set the false, default variable "foo", we will allow it after sending code process email

        String senderEmail = "elmelianimohamed6@gmail.com"; // your gmail email id
        String senderPassword = "edxewkkmdjsnzlbi"; // your gmail id password

        // Properties class enables us to connect to the host SMTP server
        Properties properties = new Properties();

        // Setting necessary information for object property

        // Setup host and mail server
        properties.put("mail.smtp.auth", "true"); // enable authentication
        properties.put("mail.smtp.starttls.enable", "true"); // enable TLS-protected connection
        properties.put("mail.smtp.host", "smtp.gmail.com"); // Mention the SMTP server address. Here Gmail's SMTP server is being used to send email
        properties.put("mail.smtp.port", "587"); // 587 is TLS port number

        // get the session object and pass username and password
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });

        try {

            MimeMessage msg = new MimeMessage(session); // Create a default MimeMessage object for compose the message

            MimeMessageHelper helper = new MimeMessageHelper(msg, true); // create MimeMessageHelper class

            helper.setFrom(new InternetAddress(senderEmail)); // adding sender email id to helper object

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); //adding recipient to msg object

            helper.setSubject("Confirmation de participation"); // adding subject to helper object

            MimeMultipart mimeMultipart = new MimeMultipart(); // create MimeMultipart object

            MimeBodyPart textMime = new MimeBodyPart(); // create first MimeBodyPart object textMime for containing the message

            MimeBodyPart messageBodyPart = new MimeBodyPart(); // create second MimeBodyPart object messageBodyPart for containing the html format data

            // create message within html format tag and assign to the content variable
            textMime.setText("");

            String content="<!--EMAIL_CONFIRMATION_EN-->\n" +
                    "<!DOCTYPE html>\n" +
                    "<html lang=\"en\" class=\"miro\" style=\"background-color:#f3f4f8;font-size:0;line-height:0\">\n" +
                    "\n" +
                    "<head xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta charset=\"UTF-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <title style=\"font-family:Helvetica,Arial,sans-serif\">Title</title>\n" +
                    "    <link rel=\"stylesheet\" href=\"../css/app.css\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "</head>\n" +
                    "\n" +
                    "<body\n" +
                    "    style=\"-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#f5f5f5;background-color:#f3f4f8;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.43;margin:0;min-width:600px;padding:0;text-align:left;width:100%!important\">\n" +
                    "    <table class=\"miro__container\" align=\"center\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\"\n" +
                    "        style=\"border-collapse:collapse;border-spacing:0;font-family:Helvetica,Arial,sans-serif;max-width:600px;min-width:600px;padding:0;text-align:left;vertical-align:top\">\n" +
                    "        <tr style=\"font-family:Helvetica,Arial,sans-serif;padding:0;text-align:left;vertical-align:top\">\n" +
                    "            <td class=\"miro__content-wrapper\"\n" +
                    "                style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.43;margin:0;padding:0;padding-top:43px;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                    "                <div class=\"miro__content\" style=\"background-color:#fff;font-family:Helvetica,Arial,sans-serif\">\n" +
                    "                    <div class=\"miro__content-body\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "                        <div class=\"miro-title-block\"\n" +
                    "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:40px 40px 36px\">\n" +
                    "                            <iiv class=\"miro-title-block__title font-size-42\"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                    "                                Comfirmation\n" +
                    "                                <div class=\"miro-title-block__subtitle font-size-20 m-top-16\"\n" +
                    "                                    style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;margin-top:16px;opacity:.6\">\n" +
                    "                                    votre demande dans le trajet ci-dessous a été confirmée<br /><br />\n" +
                    "\n" +
                    "                                   Ville depart : "+trajet.getVilleD()+"<br/><br/>\n" +
                    "                                   Ville Arrive : "+trajet.getVilleA()+"<br/><br/>\n" +
                    "                                   Date : "+trajet.getDate()+"<br/><br/>\n" +
                    "                                   Heure : "+trajet.getHeure()+"\n" +
                    "                                </div>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"miro-title-block\"\n" +
                    "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:0px 0 0px 40px\">\n" +
                    "                            <div class=\"miro-title-block__title font-size-42\"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                    "                            </div>\n" +
                    "                            <div style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.4;opacity:.6\"\n" +
                    "                                class=\"miro-title-block__subtitle font-size-20\">\n" +
                    "                            </div>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"miro-title-block\"\n" +
                    "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:0px 0 23px 40px\">\n" +
                    "                            <div class=\"miro-title-block__title font-size-42\"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                    "                            </div>\n" +
                    "                            <div class=\"miro-title-block__subtitle font-size-20 \"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;opacity:.6\">\n" +
                    "                                Pour plus d'informations nous vous invitons à contacter le propriétaire de ce trajet\n" +
                    "                            </div>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"miro__sep\"\n" +
                    "                            style=\"background-color:#e1e0e7;font-family:Helvetica,Arial,sans-serif;height:1px\"></div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "                <div class=\"miro__footer\"\n" +
                    "                    style=\"font-family:Helvetica,Arial,sans-serif;padding-bottom:72px;padding-top:42px\">\n" +
                    "\n" +
                    "                </div>\n" +
                    "            </td>\n" +
                    "        </tr>\n" +
                    "    </table>\n" +
                    "</body>\n" +
                    "\n" +
                    "</html>";

            // sets html format content to the messageBodyPart object
            messageBodyPart.setContent(content, "text/html; charset=utf-8");
            msg.setContent(mimeMultipart);
            helper.setText("");
            msg.setText("");
            //The mimeMultipart adds textMime and messageBodypart to the
            mimeMultipart.addBodyPart(textMime);
            mimeMultipart.addBodyPart(messageBodyPart);

            msg.setContent(mimeMultipart); // Set the mimeMultipart the contents of the msg

            Transport.send(msg); // Transport class send the message using send() method

            foo = true; // Set the "foo" variable to true after successfully sending emails

        } catch (Exception e) {

            System.out.println("EmailService File Error" + e);
        }

        return foo; //and return foo variable
    }

    public boolean rejectAssiste(String to, Trajet trajet) {
        boolean foo = false; // Set the false, default variable "foo", we will allow it after sending code process email

        String senderEmail = "elmelianimohamed6@gmail.com"; // your gmail email id
        String senderPassword = "edxewkkmdjsnzlbi"; // your gmail id password

        // Properties class enables us to connect to the host SMTP server
        Properties properties = new Properties();

        // Setting necessary information for object property

        // Setup host and mail server
        properties.put("mail.smtp.auth", "true"); // enable authentication
        properties.put("mail.smtp.starttls.enable", "true"); // enable TLS-protected connection
        properties.put("mail.smtp.host", "smtp.gmail.com"); // Mention the SMTP server address. Here Gmail's SMTP server is being used to send email
        properties.put("mail.smtp.port", "587"); // 587 is TLS port number

        // get the session object and pass username and password
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });

        try {

            MimeMessage msg = new MimeMessage(session); // Create a default MimeMessage object for compose the message

            MimeMessageHelper helper = new MimeMessageHelper(msg, true); // create MimeMessageHelper class

            helper.setFrom(new InternetAddress(senderEmail)); // adding sender email id to helper object

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); //adding recipient to msg object

            helper.setSubject("Confirmation de participation"); // adding subject to helper object

            MimeMultipart mimeMultipart = new MimeMultipart(); // create MimeMultipart object

            MimeBodyPart textMime = new MimeBodyPart(); // create first MimeBodyPart object textMime for containing the message

            MimeBodyPart messageBodyPart = new MimeBodyPart(); // create second MimeBodyPart object messageBodyPart for containing the html format data

            textMime.setText("");
            // create message within html format tag and assign to the content variable

           String content="<!--EMAIL_CONFIRMATION_EN-->\n" +
                   "<!DOCTYPE html>\n" +
                   "<html lang=\"en\" class=\"miro\" style=\"background-color:#f3f4f8;font-size:0;line-height:0\">\n" +
                   "\n" +
                   "<head xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                   "    <meta charset=\"UTF-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                   "    <title style=\"font-family:Helvetica,Arial,sans-serif\">Title</title>\n" +
                   "    <link rel=\"stylesheet\" href=\"../css/app.css\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                   "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                   "    <meta name=\"viewport\" content=\"width=device-width\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                   "</head>\n" +
                   "\n" +
                   "<body\n" +
                   "    style=\"-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#f5f5f5;background-color:#f3f4f8;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.43;margin:0;min-width:600px;padding:0;text-align:left;width:100%!important\">\n" +
                   "    <table class=\"miro__container\" align=\"center\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\"\n" +
                   "        style=\"border-collapse:collapse;border-spacing:0;font-family:Helvetica,Arial,sans-serif;max-width:600px;min-width:600px;padding:0;text-align:left;vertical-align:top\">\n" +
                   "        <tr style=\"font-family:Helvetica,Arial,sans-serif;padding:0;text-align:left;vertical-align:top\">\n" +
                   "            <td class=\"miro__content-wrapper\"\n" +
                   "                style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.43;margin:0;padding:0;padding-top:43px;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                   "                <div class=\"miro__content\" style=\"background-color:#fff;font-family:Helvetica,Arial,sans-serif\">\n" +
                   "                    <div class=\"miro__content-body\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                   "                        <div class=\"miro-title-block\"\n" +
                   "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:40px 40px 36px\">\n" +
                   "                            <iiv class=\"miro-title-block__title font-size-42\"\n" +
                   "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                   "                                Rejet de demande\n" +
                   "                                <div class=\"miro-title-block__subtitle font-size-20 m-top-16\"\n" +
                   "                                    style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;margin-top:16px;opacity:.6\">\n" +
                   "                                    votre demande dans le trajet ci-dessous a été rejetée<br /><br />\n" +
                   "\n" +
                   "                                     Ville depart : "+trajet.getVilleD()+"<br/><br/>\n" +
                   "                                     Ville Arrive : "+trajet.getVilleA()+"<br/><br/>\n" +
                   "                                     Date : "+trajet.getDate()+"<br/><br/>\n" +
                   "                                     Heure : "+trajet.getHeure()+"\n" +
                   "                                </div>\n" +
                   "                        </div>\n" +
                   "                        <div class=\"miro-title-block\"\n" +
                   "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:0px 0 0px 40px\">\n" +
                   "                            <div class=\"miro-title-block__title font-size-42\"\n" +
                   "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                   "                            </div>\n" +
                   "                            <div style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.4;opacity:.6\"\n" +
                   "                                class=\"miro-title-block__subtitle font-size-20\">\n" +
                   "                            </div>\n" +
                   "                        </div>\n" +
                   "                        <div class=\"miro-title-block\"\n" +
                   "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:0px 0 23px 40px\">\n" +
                   "                            <div class=\"miro-title-block__title font-size-42\"\n" +
                   "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                   "                            </div>\n" +
                   "                            <div class=\"miro-title-block__subtitle font-size-20 \"\n" +
                   "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;opacity:.6\">\n" +
                   "                                Notre plateforme est ouverte à tous, nous vous invitons à rechercher d'autres trajets\n" +
                   "                            </div>\n" +
                   "                        </div>\n" +
                   "                        <div class=\"miro__sep\"\n" +
                   "                            style=\"background-color:#e1e0e7;font-family:Helvetica,Arial,sans-serif;height:1px\"></div>\n" +
                   "                    </div>\n" +
                   "                </div>\n" +
                   "                <div class=\"miro__footer\"\n" +
                   "                    style=\"font-family:Helvetica,Arial,sans-serif;padding-bottom:72px;padding-top:42px\">\n" +
                   "\n" +
                   "                </div>\n" +
                   "            </td>\n" +
                   "        </tr>\n" +
                   "    </table>\n" +
                   "</body>\n" +
                   "\n" +
                   "</html>";

            // sets html format content to the messageBodyPart object
            messageBodyPart.setContent(content, "text/html; charset=utf-8");

            msg.setText("");

            //The mimeMultipart adds textMime and messageBodypart to the
            mimeMultipart.addBodyPart(textMime);
            mimeMultipart.addBodyPart(messageBodyPart);
            msg.setContent(mimeMultipart);
            helper.setText("");
            msg.setText("");
            msg.setContent(mimeMultipart); // Set the mimeMultipart the contents of the msg

            Transport.send(msg); // Transport class send the message using send() method

            foo = true; // Set the "foo" variable to true after successfully sending emails

        } catch (Exception e) {

            System.out.println("EmailService File Error" + e);
        }

        return foo; //and return foo variable
    }

    public boolean modifiedTrajet(String to, Trajet trajet) {
        boolean foo = false; // Set the false, default variable "foo", we will allow it after sending code process email

        String senderEmail = "elmelianimohamed6@gmail.com"; // your gmail email id
        String senderPassword = "edxewkkmdjsnzlbi"; // your gmail id password

        // Properties class enables us to connect to the host SMTP server
        Properties properties = new Properties();

        // Setting necessary information for object property

        // Setup host and mail server
        properties.put("mail.smtp.auth", "true"); // enable authentication
        properties.put("mail.smtp.starttls.enable", "true"); // enable TLS-protected connection
        properties.put("mail.smtp.host", "smtp.gmail.com"); // Mention the SMTP server address. Here Gmail's SMTP server is being used to send email
        properties.put("mail.smtp.port", "587"); // 587 is TLS port number

        // get the session object and pass username and password
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });

        try {

            MimeMessage msg = new MimeMessage(session); // Create a default MimeMessage object for compose the message

            MimeMessageHelper helper = new MimeMessageHelper(msg, true); // create MimeMessageHelper class

            helper.setFrom(new InternetAddress(senderEmail)); // adding sender email id to helper object

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); //adding recipient to msg object

            helper.setSubject("Confirmation de participation"); // adding subject to helper object

            MimeMultipart mimeMultipart = new MimeMultipart(); // create MimeMultipart object

            MimeBodyPart textMime = new MimeBodyPart(); // create first MimeBodyPart object textMime for containing the message

            MimeBodyPart messageBodyPart = new MimeBodyPart(); // create second MimeBodyPart object messageBodyPart for containing the html format data

            textMime.setText("");
            // create message within html format tag and assign to the content variable

            String content="<!--EMAIL_CONFIRMATION_EN-->\n" +
                    "<!DOCTYPE html>\n" +
                    "<html lang=\"en\" class=\"miro\" style=\"background-color:#f3f4f8;font-size:0;line-height:0\">\n" +
                    "\n" +
                    "<head xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta charset=\"UTF-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <title style=\"font-family:Helvetica,Arial,sans-serif\">Title</title>\n" +
                    "    <link rel=\"stylesheet\" href=\"../css/app.css\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "</head>\n" +
                    "\n" +
                    "<body\n" +
                    "    style=\"-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#f5f5f5;background-color:#f3f4f8;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.43;margin:0;min-width:600px;padding:0;text-align:left;width:100%!important\">\n" +
                    "    <table class=\"miro__container\" align=\"center\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\"\n" +
                    "        style=\"border-collapse:collapse;border-spacing:0;font-family:Helvetica,Arial,sans-serif;max-width:600px;min-width:600px;padding:0;text-align:left;vertical-align:top\">\n" +
                    "        <tr style=\"font-family:Helvetica,Arial,sans-serif;padding:0;text-align:left;vertical-align:top\">\n" +
                    "            <td class=\"miro__content-wrapper\"\n" +
                    "                style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.43;margin:0;padding:0;padding-top:43px;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                    "                <div class=\"miro__content\" style=\"background-color:#fff;font-family:Helvetica,Arial,sans-serif\">\n" +
                    "                    <div class=\"miro__content-body\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "                        <div class=\"miro-title-block\"\n" +
                    "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:40px 40px 36px\">\n" +
                    "                            <iiv class=\"miro-title-block__title font-size-42\"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                    "                                Modification Trajet\n" +
                    "                                <div class=\"miro-title-block__subtitle font-size-20 m-top-16\"\n" +
                    "                                    style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;margin-top:16px;opacity:.6\">\n" +
                    "                                    le trajet ci-dessous auquel vous participez a été modifié<br /><br />\n" +
                    "\n" +
                    "                                    Ville depart : "+trajet.getVilleD()+"<br/><br/>\n" +
                    "                                    Ville Arrive : "+trajet.getVilleA()+"<br/><br/>\n" +
                    "                                    Date : "+trajet.getDate()+"<br/><br/>\n" +
                    "                                    Heure : "+trajet.getHeure()+"\n" +
                    "                                </div>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"miro-title-block\"\n" +
                    "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:0px 0 0px 40px\">\n" +
                    "                            <div class=\"miro-title-block__title font-size-42\"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                    "                            </div>\n" +
                    "                            <div style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.4;opacity:.6\"\n" +
                    "                                class=\"miro-title-block__subtitle font-size-20\">\n" +
                    "                            </div>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"miro-title-block\"\n" +
                    "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:0px 0 23px 40px\">\n" +
                    "                            <div class=\"miro-title-block__title font-size-42\"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                    "                            </div>\n" +
                    "                            <div class=\"miro-title-block__subtitle font-size-20 \"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;opacity:.6\">\n" +
                    "                                notre plateforme est ouverte à tous, nous vous invitons à rechercher d'autres trajets\n" +
                    "                            </div>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"miro__sep\"\n" +
                    "                            style=\"background-color:#e1e0e7;font-family:Helvetica,Arial,sans-serif;height:1px\"></div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "                <div class=\"miro__footer\"\n" +
                    "                    style=\"font-family:Helvetica,Arial,sans-serif;padding-bottom:72px;padding-top:42px\">\n" +
                    "\n" +
                    "                </div>\n" +
                    "            </td>\n" +
                    "        </tr>\n" +
                    "    </table>\n" +
                    "</body>\n" +
                    "\n" +
                    "</html>";
            // sets html format content to the messageBodyPart object
            messageBodyPart.setContent(content, "text/html; charset=utf-8");
            msg.setContent(mimeMultipart);
            helper.setText("");
            msg.setText("");
            //The mimeMultipart adds textMime and messageBodypart to the
            mimeMultipart.addBodyPart(textMime);
            mimeMultipart.addBodyPart(messageBodyPart);

            msg.setContent(mimeMultipart); // Set the mimeMultipart the contents of the msg

            Transport.send(msg); // Transport class send the message using send() method

            foo = true; // Set the "foo" variable to true after successfully sending emails

        } catch (Exception e) {

            System.out.println("EmailService File Error" + e);
        }

        return foo; //and return foo variable
    }

    public boolean deletedTrajet(String to, Trajet trajet) {
        boolean foo = false; // Set the false, default variable "foo", we will allow it after sending code process email

        String senderEmail = "elmelianimohamed6@gmail.com"; // your gmail email id
        String senderPassword = "edxewkkmdjsnzlbi"; // your gmail id password

        // Properties class enables us to connect to the host SMTP server
        Properties properties = new Properties();

        // Setting necessary information for object property

        // Setup host and mail server
        properties.put("mail.smtp.auth", "true"); // enable authentication
        properties.put("mail.smtp.starttls.enable", "true"); // enable TLS-protected connection
        properties.put("mail.smtp.host", "smtp.gmail.com"); // Mention the SMTP server address. Here Gmail's SMTP server is being used to send email
        properties.put("mail.smtp.port", "587"); // 587 is TLS port number

        // get the session object and pass username and password
        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(senderEmail, senderPassword);
            }
        });

        try {

            MimeMessage msg = new MimeMessage(session); // Create a default MimeMessage object for compose the message

            MimeMessageHelper helper = new MimeMessageHelper(msg, true); // create MimeMessageHelper class

            helper.setFrom(new InternetAddress(senderEmail)); // adding sender email id to helper object

            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to)); //adding recipient to msg object

            helper.setSubject("Confirmation de participation"); // adding subject to helper object

            MimeMultipart mimeMultipart = new MimeMultipart(); // create MimeMultipart object

            MimeBodyPart textMime = new MimeBodyPart(); // create first MimeBodyPart object textMime for containing the message

            MimeBodyPart messageBodyPart = new MimeBodyPart(); // create second MimeBodyPart object messageBodyPart for containing the html format data

            textMime.setText("");
            // create message within html format tag and assign to the content variable

            String content="<!--EMAIL_CONFIRMATION_EN-->\n" +
                    "<!DOCTYPE html>\n" +
                    "<html lang=\"en\" class=\"miro\" style=\"background-color:#f3f4f8;font-size:0;line-height:0\">\n" +
                    "\n" +
                    "<head xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta charset=\"UTF-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <title style=\"font-family:Helvetica,Arial,sans-serif\">Title</title>\n" +
                    "    <link rel=\"stylesheet\" href=\"../css/app.css\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "</head>\n" +
                    "\n" +
                    "<body\n" +
                    "    style=\"-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#f5f5f5;background-color:#f3f4f8;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.43;margin:0;min-width:600px;padding:0;text-align:left;width:100%!important\">\n" +
                    "    <table class=\"miro__container\" align=\"center\" width=\"600\" cellpadding=\"0\" cellspacing=\"0\"\n" +
                    "        style=\"border-collapse:collapse;border-spacing:0;font-family:Helvetica,Arial,sans-serif;max-width:600px;min-width:600px;padding:0;text-align:left;vertical-align:top\">\n" +
                    "        <tr style=\"font-family:Helvetica,Arial,sans-serif;padding:0;text-align:left;vertical-align:top\">\n" +
                    "            <td class=\"miro__content-wrapper\"\n" +
                    "                style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;hyphens:auto;line-height:1.43;margin:0;padding:0;padding-top:43px;text-align:left;vertical-align:top;word-wrap:break-word\">\n" +
                    "                <div class=\"miro__content\" style=\"background-color:#fff;font-family:Helvetica,Arial,sans-serif\">\n" +
                    "                    <div class=\"miro__content-body\" style=\"font-family:Helvetica,Arial,sans-serif\">\n" +
                    "                        <div class=\"miro-title-block\"\n" +
                    "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:40px 40px 36px\">\n" +
                    "                            <iiv class=\"miro-title-block__title font-size-42\"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                    "                                Suppression Trajet\n" +
                    "                                <div class=\"miro-title-block__subtitle font-size-20 m-top-16\"\n" +
                    "                                    style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;margin-top:16px;opacity:.6\">\n" +
                    "                                    le trajet ci-dessous auquel vous participez a été supprimé<br /><br />\n" +
                    "\n" +
                    "                                    Ville depart : "+trajet.getVilleD()+"<br/><br/>\n" +
                    "                                    Ville Arrive : "+trajet.getVilleA()+"<br/><br/>\n" +
                    "                                    Date : "+trajet.getDate()+"<br/><br/>\n" +
                    "                                    Heure : "+trajet.getHeure()+"\n" +
                    "                                </div>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"miro-title-block\"\n" +
                    "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:0px 0 0px 40px\">\n" +
                    "                            <div class=\"miro-title-block__title font-size-42\"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                    "                            </div>\n" +
                    "                            <div style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.4;opacity:.6\"\n" +
                    "                                class=\"miro-title-block__subtitle font-size-20\">\n" +
                    "                            </div>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"miro-title-block\"\n" +
                    "                            style=\"background-position:center;background-repeat:no-repeat;background-size:100% auto;font-family:Helvetica,Arial,sans-serif;padding:0px 0 23px 40px\">\n" +
                    "                            <div class=\"miro-title-block__title font-size-42\"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:42px!important;font-stretch:normal;font-style:normal;font-weight:700;letter-spacing:normal;line-height:1.24\">\n" +
                    "                            </div>\n" +
                    "                            <div class=\"miro-title-block__subtitle font-size-20 \"\n" +
                    "                                style=\"color:#050038;font-family:Helvetica,Arial,sans-serif;font-size:20px!important;font-stretch:normal;font-style:normal;font-weight:400;letter-spacing:normal;line-height:1.4;opacity:.6\">\n" +
                    "                                notre plateforme est ouverte à tous, nous vous invitons à rechercher d'autres trajets\n" +
                    "                            </div>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"miro__sep\"\n" +
                    "                            style=\"background-color:#e1e0e7;font-family:Helvetica,Arial,sans-serif;height:1px\"></div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "                <div class=\"miro__footer\"\n" +
                    "                    style=\"font-family:Helvetica,Arial,sans-serif;padding-bottom:72px;padding-top:42px\">\n" +
                    "\n" +
                    "                </div>\n" +
                    "            </td>\n" +
                    "        </tr>\n" +
                    "    </table>\n" +
                    "</body>\n" +
                    "\n" +
                    "</html>";
            // sets html format content to the messageBodyPart object
            messageBodyPart.setContent(content, "text/html; charset=utf-8");

            //The mimeMultipart adds textMime and messageBodypart to the
            mimeMultipart.addBodyPart(textMime);
            mimeMultipart.addBodyPart(messageBodyPart);
            msg.setText("");
            msg.setContent(mimeMultipart); // Set the mimeMultipart the contents of the msg

            Transport.send(msg); // Transport class send the message using send() method

            foo = true; // Set the "foo" variable to true after successfully sending emails

        } catch (Exception e) {

            System.out.println("EmailService File Error" + e);
        }

        return foo; //and return foo variable
    }
}
