package com.example.utilitis;

import lombok.Data;

import java.time.LocalDate;

@Data
public class modifyLoggedForm {
    String nom;
    LocalDate date_naissance;
    String adresse;
    String tel;
    String numPermis;
    String email;
}
