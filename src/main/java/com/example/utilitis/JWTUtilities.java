package com.example.utilitis;

public class JWTUtilities {
    public static final String SECRET="Mysecret1234";
    public static final String PREFIX="Bearer ";
    public static final String AUTH_HEADER="Authorization";
    public static final long EXPIRE_ACCESS_TOKEN=15*60*1000;
    public static final long EXPIRE_REFRESH_TOKEN=60*24*60*1000;
}
