package com.example.service;

import com.example.dao.TrajetRepo;
import com.example.entities.Trajet;
import com.example.entities.Utilisateur;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class TrajetServiceImpl implements TrajetService{
    private TrajetRepo trajetRepo;

    @Override
    public List<Trajet> trajetParVilleDetVilleAetdateD(String villeD, String villeA, LocalDate dateD) {
        return trajetRepo.findByVilleDAndVilleAAndDate(villeD,villeA,dateD);
    }

    @Override
    public Trajet addTrajet(Trajet trajet, Utilisateur owner) {
        return trajetRepo.save(trajet);
    }

    @Override
    public List<Trajet> trajetsofLogged(Utilisateur user) {
        return trajetRepo.findByCarOwner(user);
    }
}
