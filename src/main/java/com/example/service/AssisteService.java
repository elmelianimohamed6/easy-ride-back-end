package com.example.service;

import com.example.entities.Assiste;
import com.example.entities.Trajet;
import com.example.entities.Utilisateur;
import com.example.utilitis.AssisteUtilitise;

public interface AssisteService {
    Assiste addAssiste(Utilisateur user, Trajet trajet);
    Assiste acceptedAssiste(Utilisateur user,Trajet trajet);
    Assiste refuseAssiste(Utilisateur user,Trajet trajet);
}
