package com.example.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.example.dao.RoleRepo;
import com.example.dao.UtilisateurRepo;
import com.example.entities.Role;
import com.example.entities.Utilisateur;
import com.example.utilitis.JWTUtilities;
import com.example.utilitis.modifyLoggedForm;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class UtilisateurServiceImpl implements UtilisateurService {
    private UtilisateurRepo utilisateurRepo;
    private RoleRepo roleRepo;
    private PasswordEncoder encoder;
    @Override
    public Utilisateur addNewUser(Utilisateur user) {
        Utilisateur utilisateur=loadUserByUsername(user.getEmail());
        if(utilisateur==null){
            user.setPassword(encoder.encode(user.getPassword()));
            user.getRoles().add(roleRepo.findByName("USER"));
            return utilisateurRepo.save(user);
        }
        else
            throw new RuntimeException("Utilisateur existe deja");
    }

    @Override
    public Role addNewRole(Role role) {
        return  roleRepo.save(role);
    }

    @Override
    public void addRoleToUser(String email, String roleName) {
        Utilisateur user=utilisateurRepo.findByEmail(email);
        Role role=roleRepo.findByName(roleName);
        user.getRoles().add(role);
    }

    @Override
    public Utilisateur loadUserByUsername(String email) {
         return  utilisateurRepo.findByEmail(email);
    }

    @Override
    public Utilisateur modifyLogged(Utilisateur utilisateur,modifyLoggedForm u) {
        utilisateur.setId(utilisateur.getId());
        utilisateur.setDate_naissance(u.getDate_naissance());
        utilisateur.setNom(u.getNom());
        utilisateur.setAdresse(u.getAdresse());
        utilisateur.setTel(u.getTel());
        utilisateur.setEmail(u.getEmail());
        return utilisateurRepo.save(utilisateur);
    }

    @Override
    public List<Utilisateur> listUsers() {
        return utilisateurRepo.findAll();
    }

    @Override
    public boolean checkPassword(String email, String password) {
        Utilisateur user=loadUserByUsername(email);
        return encoder.matches(password, user.getPassword());
    }

    @Override
    public void buildTokenAfterUpdate(modifyLoggedForm u, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Utilisateur user= loadUserByUsername(u.getEmail());
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(r -> {
            authorities.add(new SimpleGrantedAuthority(r.getName()));
        });
        User user1=new User(user.getEmail(), user.getPassword(), authorities);

        Algorithm algorithm=Algorithm.HMAC256(JWTUtilities.SECRET);
        String access_token= JWT.create()
                .withSubject(user1.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+JWTUtilities.EXPIRE_ACCESS_TOKEN))
                .withIssuer(request.getRequestURL().toString())
                .withClaim("roles",user1.getAuthorities().stream().map(grantedAuthority -> grantedAuthority.getAuthority()).collect(Collectors.toList()))
                .sign(algorithm);

        String refresh_token= JWT.create()
                .withSubject(user1.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+JWTUtilities.EXPIRE_REFRESH_TOKEN))
                .withIssuer(request.getRequestURL().toString())
                .sign(algorithm);

        Map<String,String> tokens=new HashMap<>();
        tokens.put("access_token",access_token);
        tokens.put("refresh_token",refresh_token);
        response.setContentType("application/json");
        new ObjectMapper().writeValue(response.getOutputStream(),tokens);
    }
}
