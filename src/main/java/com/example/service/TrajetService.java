package com.example.service;

import com.example.entities.Trajet;
import com.example.entities.Utilisateur;

import java.time.LocalDate;
import java.util.List;

public interface TrajetService {
    List<Trajet> trajetParVilleDetVilleAetdateD(String villeD, String villeA, LocalDate dateD);
    Trajet addTrajet(Trajet trajet, Utilisateur owner);
    List<Trajet> trajetsofLogged(Utilisateur user);
}
