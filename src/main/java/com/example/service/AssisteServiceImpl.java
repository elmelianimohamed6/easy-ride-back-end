package com.example.service;

import com.example.dao.AssisteRepo;
import com.example.dao.TrajetRepo;
import com.example.entities.Assiste;
import com.example.entities.AssisteKey;
import com.example.entities.Trajet;
import com.example.entities.Utilisateur;
import com.example.utilitis.AssisteUtilitise;
import com.example.utilitis.EmailSenderService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class AssisteServiceImpl implements AssisteService{

    AssisteRepo assisteRepo;
    EmailSenderService senderService;
    TrajetRepo repo;

    @Override
    public Assiste addAssiste(Utilisateur user, Trajet trajet) {
        AssisteKey key=new AssisteKey(user.getId(),trajet.getId());
        senderService.sendDemandeAssiste(trajet.getCar().getOwner().getEmail(),user,trajet);
        return assisteRepo.save(new Assiste(key,user,trajet, AssisteUtilitise.HOLD));
    }

    @Override
    public Assiste acceptedAssiste(Utilisateur user,Trajet trajet) {
        Assiste a=assisteRepo.getById(new AssisteKey(user.getId(),trajet.getId()));
        a.setKey(new AssisteKey(user.getId(),trajet.getId()));
        trajet.setId(trajet.getId());
        trajet.setNbrePlace(trajet.getNbrePlace()-1);
        repo.save(trajet);
        a.setStatus(AssisteUtilitise.ACCEPTED);
        senderService.confirmAssiste(user.getEmail(),trajet);
        return assisteRepo.save(a);
    }

    @Override
    public Assiste refuseAssiste(Utilisateur user, Trajet trajet) {
        Assiste a=assisteRepo.getById(new AssisteKey(user.getId(),trajet.getId()));
        a.setKey(new AssisteKey(user.getId(),trajet.getId()));
        a.setStatus(AssisteUtilitise.DENIED);
        senderService.rejectAssiste(user.getEmail(),trajet);
        return assisteRepo.save(a);
    }
}
