package com.example.service;

import com.example.entities.Role;
import com.example.entities.Utilisateur;
import com.example.utilitis.modifyLoggedForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface UtilisateurService {
    Utilisateur addNewUser(Utilisateur user);
    Role addNewRole(Role role);
    void addRoleToUser(String email,String roleName);
    Utilisateur loadUserByUsername(String email);
    Utilisateur modifyLogged(Utilisateur utilisateur,modifyLoggedForm u);
    List<Utilisateur> listUsers();
    boolean checkPassword(String email,String password);
    void buildTokenAfterUpdate(modifyLoggedForm u, HttpServletRequest request, HttpServletResponse response) throws IOException;
}
