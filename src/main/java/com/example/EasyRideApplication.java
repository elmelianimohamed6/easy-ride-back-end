package com.example;

import com.example.dao.AssisteRepo;
import com.example.dao.TrajetRepo;
import com.example.dao.UtilisateurRepo;
import com.example.dao.VoitureRepo;
import com.example.entities.*;
import com.example.service.UtilisateurService;
import com.example.utilitis.AssisteUtilitise;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.LocalDate;


@SpringBootApplication
@AllArgsConstructor
public class EasyRideApplication implements WebMvcConfigurer {

    UtilisateurRepo repo;
    VoitureRepo voitureRepo;
    TrajetRepo trajetRepo;
    AssisteRepo assisteRepo;

    public static void main(String[] args) {
        SpringApplication.run(EasyRideApplication.class, args);
    }

    @Bean
    CommandLineRunner start(UtilisateurService service) {
        return args -> {

            service.addNewRole(new Role("USER"));
            service.addNewRole(new Role("ADMIN"));

            Utilisateur user1 = service.addNewUser(new Utilisateur("mohamed", "test", "0626219345", "elmelianimohamed6@gmail.com", "test"));
            Utilisateur user3 = service.addNewUser(new Utilisateur("mohamed", "test", "0626219345", "email1", "test"));
            Utilisateur user2 = service.addNewUser(new Utilisateur("mohamed", "test", "0626219345", "elahmadikarim2@gmail.com", "test"));
            service.addRoleToUser("elmelianimohamed6@gmail.com", "ADMIN");
            Voiture v = voitureRepo.save(new Voiture("Mat3", "2019", "SEDAN", "BLACK", 4, user1));
            trajetRepo.save(new Trajet("Casablanca", "Marrakech", LocalDate.of(2022, 6, 12), "12:30", 3, 105,true, v));
            v = voitureRepo.save(new Voiture("Mat1", "2019", "SEDAN", "BLACK", 4, user1));
            trajetRepo.save(new Trajet("Casablanca", "Marrakech", LocalDate.of(2022, 6, 12), "12:30", 5, 15,false, v));
            Trajet t =trajetRepo.save(new Trajet("Casablanca", "Marrakech", LocalDate.of(2022, 6, 12), "12:30", 5, 15,true, v));
            Assiste a = assisteRepo.save(new Assiste(new AssisteKey(user2.getId(),t.getId()),user2,t, AssisteUtilitise.HOLD));
            assisteRepo.save(new Assiste(new AssisteKey(user3.getId(),t.getId()),user3,t, AssisteUtilitise.HOLD));
        };

    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins(
                        "*"
                )
                .allowedMethods(
                        "GET",
                        "PUT",
                        "POST",
                        "DELETE",
                        "PATCH",
                        "OPTIONS"
                );
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

