package com.example.entities;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor @RequiredArgsConstructor
public class Trajet implements Serializable {
    @Id  @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;
    @NonNull
    private String villeD;
    @NonNull
    private String villeA;
    @NonNull
    private LocalDate date;
    @NonNull
    private String heure;
    @NonNull
    private int nbrePlace;
    @NonNull
    private double prix;
    @NonNull
    private boolean isHighway;
    @NonNull
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Voiture car;
    @OneToMany(mappedBy = "trajet",fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection<Assiste> assistes;
}
