package com.example.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @RequiredArgsConstructor
public class Voiture implements Serializable {
    @Id @NonNull
    private String mat;
    @NonNull
    private String annee;
    @NonNull
    private String cat;
    @NonNull
    private String color;
    @NonNull
    private int nbrePlace;
    @ManyToOne
    @JoinColumn(name = "id_owner")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Utilisateur owner;
}
