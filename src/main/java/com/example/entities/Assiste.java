package com.example.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor
public class Assiste implements Serializable {
    @EmbeddedId
    private AssisteKey key;
    @JsonIgnore
    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name = "user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Utilisateur utilisateur;
    @JsonIgnore
    @ManyToOne
    @MapsId("trajetId")
    @JoinColumn(name = "trajet_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Trajet trajet;
    private String status;
}
