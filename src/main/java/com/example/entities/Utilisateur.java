package com.example.entities;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @RequiredArgsConstructor
public class Utilisateur implements Serializable {
    @Id @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;
    @NonNull
    private String nom;
    private LocalDate date_naissance;
    @NonNull
    private String adresse;
    @NonNull
    private String tel;
    @NonNull
    @Column(nullable = false,unique = true)
    private String email;
    @NonNull
    @Column(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @Lob
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "profilePic", referencedColumnName = "id")
/*    @JsonIgnoreProperties({"fileName", "fileType","data"})*/
    private Attachment photoProfile;
    private final LocalDate dateCmpt = LocalDate.now();
    private String token;
    private boolean isActive;
    private boolean isVerified=false;
    private long expireAt;
    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Collection<Role> Roles=new ArrayList<>();
}
